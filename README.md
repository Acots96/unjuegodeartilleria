# PEC3

Este es un juego de plataformas (y un poco de artillería) basado en el clásico videojuego Super Mario Bros. Concretamente en el primer nivel.

Consta de una sola escena con los siguientes elementos:
 - Personaje jugable de Mario.
 - Enemigos (únicamente los que tienen forma de champiñón).
 - Powerups (únicamente la flor).
 - Monedas
 - Bloques del suelo.
 - Bloques de plataformas (ladrillo e interrogación).
 - Tuberías verdes.
 - Poste con bandera.
 - Castillo al final del nivel.
 - Elementos decorativos (montañas, arbustos y nubes).
 - Bola de fuego (utilizada por Mario para atacar).
 - Sistemas de partículas para la bola de fuego.
 - CheckPoints.
 
También tiene los 4 elementos de la UI:
 - Puntuación del jugador.
 - Monedas obtenidas.
 - Indicador del nivel.
 - Tiempo restante.

Enlace al video de muestra: https://youtu.be/Sh-h7BcpWV0


# Estructuración e implementación

El nivel está estructurado tratando de imitar la estética del nivel original, por lo que todos los elementos del terreno están en la misma posición, sino parecida.

El juego consta de diversas clases:
 - GameManager: Controlador principal del juego que se encarga básicamente de controlar y actualizar los elementos de la UI, de controlar el MusicManager para los sonidos y la canción de fondo, de proporcionar el Colectable (powerup o moneda) al CubeController y finalmente de llevar a cabo las acciones pertinentes cuando el jugador gana. Si por el contrario pierde, comprueba el último CheckPoint del jugador y, si tiene monedas lo revive en el último CheckPoint, sino al principio del nivel.
 - MusicManager: Controlador que provee a GameManager de las funciones necesarias para ejecutar los sonidos correspondientes en cada momento.
 - PlayerController: Controlador del jugador que mueve al personaje de Mario según el input recibido del jugador. También se encarga de comprobar sus colisiones con los demás elementos como los enemigos, los bloques (suelo y plataformas) y los Colliders pertinentes de ganar y morir. Cada elemento tiene un layer/tag concreto que utiliza el script para diferenciar unos elementos de otros y tomar la decisión correspondiente. Finalmente gestiona los powerups y comunica al GameController cada acción realizada dado que la mayoría de las acciones conllevan cambios en la UI (más puntuación).
 - EnemyController: Controlador de los enemigos que mueve cada personaje horizontalmente utilizando un collider con un layer concreto para hacerlo cambiar de dirección. El enemigo tiene un comportamiento distinto dependiendo de la parte que colisione con Mario: Si colisiona con el enemigo por encima, este muere, mientras que si colisiona lateralmente Mario muere. Para ello he creado 3 colliders distintos para gestionar la interacción de Mario con cada enemigo. Además, los enemigos tienen un pequeño añadido de IA para dirigirse hacia la posición de Mario cuando está lo suficientemente cerca (horizontalmente solo, no saltan).
 - CubeController: Controlador de los bloques de las plataformas, que gestiona la colisión de Mario con la parte inferior de cada bloque: utiliza un rigidbody para hacer "saltar" al bloque e instancia el Colectable (aleatorio) obtenido de GameController.
 - CameraController: Controlador de la cámara, cuyo único objetivo es el de seguir a Mario horizontalmente, siendo bloqueada al principio para evitar desplazarse hacia la izquierda si Mario se mueve hacia esa direccón.
 - FireBallController: Controlador de la bola de fuego lanzada por Mario cuando está en estado súper. Al instanciarla se le da un impulso horizontal. El prefab contiene dos ParticleSystem: uno con forma esférica pero radio muy pequeño (que emite partículas con el movimiento), el cual simula una estela cuando la bola se está moviendo. El otro ParticleSystem es similar al anterior pero tiene un radio más grande y emite partículas en el tiempo, por lo que genera un efecto de explosión. También dispone de un matereial físico con un Bounciness mínimo para rebotar lo suficiente sin ser exagerado.
 
Un detalle importante a destacar respecto a la entrega de Mario anterior, es que para solucionar ciertos problemas (como el pequeño bouncing al caer en una plataforma o el choque lateral con los bloques) se han tenido que añadir algunas modificaciones:
 - Mario se mueve ahora mediante el uso del Rigidbody2D en lugar de mover el Transform manualmente, ya que este último método no tiene en cuenta los Colliders2D adecuadamente.
 - Detection del Rigidbody2D de Mario ahora es Continuous en lugar de Discrete, cosa que evita el pequeño salto que daba Mario al caer en los bloques.
 - Aplicado el uso de un PhysicMaterial2D con Friction=0 para evitar que Mario choque horizontalmente con los bloques y se quede enganchado.
 - Ahora Mario usa dos Colliders2D: CapsuleCollider2D para no quedarse enganchado al pasar por encima de los bloques de las plataformas; BoxCollider2D para no deslizarse y caer por los lados de las plataformas de bloques.
 - Hay tres CheckPoints repartidos por el nivel, siendo el primero de ellos donde aparece Mario al principio. Cuando Mario toca un CheckPoint este se guarda siempre que esté más avanzado en el nivel que el anterior CheckPoint guardado. Cuando mario muere, si tiene monedas aparecerá en el último CheckPoint, sino tiene monedas se reiniciará el juego.

Para conocer más en detalle la implementación echa un vistazo a las cabeceras de las clases y sus métodos.


