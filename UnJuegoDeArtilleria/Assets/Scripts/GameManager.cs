﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Clase que controla el comportamiento general del juego y los elementos de la UI. 
 */
public class GameManager : MonoBehaviour {

    public GameObject[] CollectablesPrefab;
    public PlayerController Player;
    public Animation FlagAnim;
    public MusicManager Music;

    public Text PointsText, CoinsText, TimeText, GameOverText;
    public int KillEnemyPoints, CatchPowerupPoints, BreakBlockPoints, WinPoints, EachSecondPoints, Time;

    private int points, coins, time;
    private bool isGameOver, isReloadScene, isFlagDown;


    private void Awake() {
        time = Time;
        coins = points = 0;
        PointsText.text = 0 + "";
        CoinsText.text = 0 + "";
        isGameOver = false;
        InvokeRepeating("AddTimeEffect", 0f, 1f);
    }


    /**
     * Metodo utilizado por los bloques cuando
     * muestran una moneda o un powerup.
     */
    public GameObject GetRandomCollectable() {
        return CollectablesPrefab[Random.Range(0, CollectablesPrefab.Length)];
    }


    /**
     * Meotodos que suman puntos por las acciones
     * de matar enemigos, coger powerups y romper bloques.
     */
    public void KillEnemy() {
        Music.PlayKillSound();
        points += KillEnemyPoints;
        PointsText.text = points + "";
    }
    public void CatchPowerup() {
        Music.PlayPowerupSound();
        points += CatchPowerupPoints;
        PointsText.text = points + "";
    }
    public void BreakBlock() {
        Music.PlayBreakBlockSound();
        points += BreakBlockPoints;
        PointsText.text = points + "";
    }

    /**
     * Corutina que genera el efecto de sumar puntos
     * segun la cantidad de tiempo restante, en lugar
     *  de sumarlos todos de golpe.
     *  Recarga la escena para empezar de nuevo.
     */
    private IEnumerator AddPointsEffect() {
        while (time > 0) {
            TimeText.text = --time + "";
            points += EachSecondPoints;
            PointsText.text = points + "";
            yield return null;
        }
        GameOverText.text = "VOLVIENDO A EMPEZAR...";
        yield return new WaitForSeconds(3f);
        ReloadScene();
    }

    public void AddCoin() {
        Music.PlayCoinSound();
        CoinsText.text = ++coins + "";
    }

    /**
     * Metodo que se repite cada segundo para actualizar
     * el contador de tiempo y, si llega a 0, indicar
     * que ha perdido la partida.
     */
    private void AddTimeEffect() {
        if (!isGameOver) {
            if (time > 0)
                TimeText.text = --time + "";
            else
                Player.Die();
        }
    }


    
    public void FlagDown() {
        if (isFlagDown)
            return;
        isFlagDown = true;
        FlagAnim.Play();
        points += WinPoints;
        PointsText.text = points + "";
    }
    public void Win() {
        Music.StopSong();
        Music.PlayWinSound();
        isGameOver = true;
        GameOverText.text = "HAS GANADO!";
        isReloadScene = true;
        StartCoroutine(AddPointsEffect());
    }
    public void Die() {
        Music.StopSong();
        Music.PlayDieSound();
        isGameOver = true;
        isReloadScene = coins == 0;
        GameOverText.text = isReloadScene ? "HAS PERDIDO :(" : "";
        StartCoroutine(LoseAmountEffect());
    }
    /**
     * Cada vez que muera el jugador perdera 5 monedas y 100 puntos
     * (siempre quedando 0 como minimo, nunca negativo), y 
     * reaparecerá en el ultimo CheckPoint, divididos en:
     * - Al principio del nivel.
     * - Antes del primer hueco de caida.
     * - Entre la segunda y tercera escalera.
     * 
     * Si el jugador no tiene monedas entonces al morir volvera al
     * primer principio (primer CheckPoint), reiniciando la escena,
     * sino aparecera en el ultimo checkpoint que haya guardado
     * (por defecto el primero es el inicio del juego).
     */
    public void ReloadScene() {
        if (isReloadScene) {
            SceneManager.LoadScene("SampleScene");
        } else {
            Player.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            Player.Init();
            Player.transform.position = Player.lastCheckPoint.position;
            Music.PlaySong();
        }
    }

    /**
     * Corutina para simular el efecto de pérdida de puntos y monedas.
     */
    private IEnumerator LoseAmountEffect() {
        float coinsToReach = Mathf.Max(coins - 5, 0);
        float pointsToReach = Mathf.Max(points - 100, 0);
        while (coins > coinsToReach || points > pointsToReach) {
            if (coins > coinsToReach)
                CoinsText.text = --coins + "";
            if (points > pointsToReach)
                PointsText.text = (points -= 1) + "";
            yield return null;
        }
        Invoke("ReloadScene", 2f);
    }


    public void PlayJumpSound() {
        Music.PlayJumpSound();
    }
}
