﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/**
 * Clase que controla el funcionamiento de los bloques 
 * de interrogacion y los normales.
 * 
 * Esta compuesto de una plataforma en la parte inferior 
 * que el player acciona al saltar desde abajo y provoca
 * el empuje del bloque hacia arriba.
 * 
 * Cuando el bloque vuelve a su posición inicial pide 
 * al Manager un objeto para mostrar (moneda o powerup).
 */
public class CubeController : MonoBehaviour {

    public Rigidbody2D CubeRb;
    public Transform CollectablePosition;
    public float ThrowUpForce;
    public Animator Animator;

    public GameManager Manager;
    private GameObject Collectable;

    private bool containsObject, alreadyThrown, wasFalling;


    private void Awake() {
        containsObject = true;
        alreadyThrown = false;
    }


    private void Start() {
        Collectable = Manager.GetRandomCollectable();
    }



    /**
     * Empuja el bloque hacia arriba solo si no 
     * ha sido lanzado ya.
     * 
     * Lanza una corutina que espera hasta que el bloque
     * esta cayendo para que solo se muestre la moneda
     * o el powerup cuando el bloque ha acabado de caer.
     * 
     * Utiliza "Animator" como booleano para saber
     * si es un bloque de interrogacion (tiene AnimatorController)
     * o un bloque normal (no tiene AnimatorController).
     */
    private void ThrowUp() {
        if (alreadyThrown)
            return;
        CubeRb.isKinematic = false;
        CubeRb.AddForce(new Vector2(0f, ThrowUpForce));
        StartCoroutine(CheckFalling());
        alreadyThrown = true;
        if (Animator != null)
            Animator.SetTrigger("DoEmpty");
    }

    private void ShowCollectable() {
        Instantiate(Collectable, CollectablePosition.position, Quaternion.identity);
    }


    /**
     * - Si choca con el player debe lanzar hacia arriba el bloque.
     * - Si el player es Super (y no es bloque de interrogacion) 
     * entonces rompe el bloque.
     * 
     * - Por otro lado, cuando cae despues de haber sido lanzado 
     * choca con la plataforma de nuevo, entonces muestra el colectable.
     */
    private void OnCollisionEnter2D(Collision2D collision) {
        GameObject other = collision.collider.gameObject;
        if (other.layer == LayerMask.NameToLayer("Player")) {
            if (Animator == null && other.GetComponent<PlayerController>().IsSuper) {
                Manager.BreakBlock();
                Destroy(gameObject);
            } else {
                ThrowUp();
            }
        } else {
            if (alreadyThrown && containsObject && wasFalling) {
                CubeRb.isKinematic = true;
                ShowCollectable();
                containsObject = wasFalling = false;
            }
        }
    }



    /**
     * Corutina que espera hasta que la velocidad 
     * del cubo sea inferior a 0 para actualizar el booleano.
     */
    private IEnumerator CheckFalling() {
        yield return new WaitUntil(() => CubeRb.velocity.y < 0);
        wasFalling = true;
    }
}
