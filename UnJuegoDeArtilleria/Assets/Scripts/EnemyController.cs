﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase que controla el comportamiento del enemigo.
 * 
 * Tiene dos colliders horizontales que matan al player 
 * si los toca. Por otro lado tiene un collider encima
 * que mata al enemigo si el player lo toca.
 */
public class EnemyController : MonoBehaviour {

    public Collider2D bodyCollider, upCollider, leftCollider, rightCollider;
    private Animator animator;
    private Rigidbody2D rb;
    private Transform thisTr;

    public GameManager Manager;
    private Transform player;

    public float speed, distanceToChase;
    private Vector3 direction;
    public bool IsDead { get; private set; }


    private void Awake() {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        direction = Vector3.left;
        thisTr = transform;
        IsDead = false;
        player = Manager.Player.transform;
    }

    // Update is called once per frame
    private void Update() {
        float thisX = thisTr.position.x, playerX = player.position.x;
        if (Mathf.Abs(thisX - playerX) <= distanceToChase) {
            direction.x = playerX < thisX ? -1 : 1;
        }
        thisTr.position += direction * speed * Time.deltaTime;
    }


    /**
     * Cuando el player choca con el enemigo le pregunta
     * que collider ha tocado.
     * Si resulta ser el que esta encima del enemigo entonces
     * este muere.
     * Si por el contrario es uno lateral se lo comunica al player
     * y este ya decide que hacer.
     */
    public bool IsDeadlyCollider(Collider2D c) {
        if (c.Equals(upCollider)) {
            Die();
            Manager.KillEnemy();
            return false;
        } else {
            return true;
        }
    }


    public void Die() {
        IsDead = true;
        bodyCollider.enabled = upCollider.enabled = leftCollider.enabled = rightCollider.enabled = false;
        speed = 0;
        rb.gravityScale = 0;
        rb.velocity = Vector2.zero;
        animator.SetTrigger("DoDie");
        Destroy(gameObject, 1.5f);
    }


    /**
     * Cuando choca con un obstaculo, como una tuberia,
     * modifica su direccion.
     */
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.CompareTag("Obstacle"))
            direction.x *= -1;
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Obstacle"))
            direction.x *= -1;
    }
}
