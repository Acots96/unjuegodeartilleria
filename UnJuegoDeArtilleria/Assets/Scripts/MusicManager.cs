﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase que contiene y controla los sonidos y la cancion
 */
public class MusicManager : MonoBehaviour {

    public AudioSource Song;

    public AudioSource JumpSound, WinSound, LoseSound, 
        DieSound, KillSound, PowerupSound, CoinSound, BreakBlock;


    public void PlaySong() {
        Song.Play();
    }
    public void StopSong() {
        Song.Stop();
    }


    public void PlayJumpSound() {
        JumpSound.PlayOneShot(JumpSound.clip);
    }
    public void PlayWinSound() {
        WinSound.PlayOneShot(WinSound.clip);
    }
    public void PlayLoseSound() {
        LoseSound.PlayOneShot(LoseSound.clip);
    }
    public void PlayDieSound() {
        DieSound.PlayOneShot(DieSound.clip);
    }
    public void PlayKillSound() {
        KillSound.PlayOneShot(KillSound.clip);
    }
    public void PlayPowerupSound() {
        PowerupSound.PlayOneShot(PowerupSound.clip);
    }
    public void PlayCoinSound() {
        CoinSound.PlayOneShot(CoinSound.clip);
    }
    public void PlayBreakBlockSound() {
        BreakBlock.PlayOneShot(BreakBlock.clip);
    }

}
