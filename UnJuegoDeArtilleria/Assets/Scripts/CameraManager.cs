﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase que controla el movimiento de la camara.
 */
public class CameraManager : MonoBehaviour {
    
    public Transform target;

    private Transform thisTr;
    private float limitX;


    private void Awake() {
        thisTr = transform;
        limitX = thisTr.position.x;
    }


    /**
     * Avanza lateralmente cuando el player se desplaza.
     */
    void LateUpdate() {
        Vector3 pos = thisTr.position;
        if (target.position.x > limitX)
            pos.x = target.position.x;
        thisTr.position = pos;
    }
}
