﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase que controla el comportamiento de la bola de fuego que lanza Mario.
 */
public class FireBallController : MonoBehaviour {

    public float force;
    public GameObject destroyEffectPrefab;

    private Animator animator;
    private Rigidbody2D rb;
    private BoxCollider2D box;

    private bool alreadyExploded;
    private GameObject explodeEffect;


    private void Awake() {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.right * force);
        box = GetComponent<BoxCollider2D>();
    }


    /**
     * Solo entra al metodo si no ha sido llamado ya, 
     * dado que es posible por la implementacion en el
     * metodo OnCollisionEnter2D.
     * Al explotar desactiva todo para evitar que choque
     * con otros colliders o caiga al vacio, y
     * finalmente instancia el ParticleSystem que simula
     * el efecto de explosion.
     */
    private void Explode() {
        if (alreadyExploded)
            return;
        animator.SetTrigger("DoExplode");
        box.enabled = false;
        rb.gravityScale = 0;
        rb.velocity = Vector2.zero;
        explodeEffect = Instantiate(destroyEffectPrefab, transform.position, Quaternion.identity);
        alreadyExploded = true;
    }


    /**
     * AnimationEvent llamado al terminar la
     * animacion de explotar para eliminar
     * este gameObject.
     */
    public void DestroyAnimationEvent() {
        Destroy(gameObject);
    }


    /**
     * Si choca con un enemigo lo mata.
     * Si por el contrario toca el suelo primero
     * entonces espera un segundo y medio para explotar,
     * pero no necesariamente mata a ningun enemigo, 
     * solo cuando los toca.
     */
    private void OnCollisionEnter2D(Collision2D collision) {
        int layer = collision.collider.gameObject.layer;
        if (layer == LayerMask.NameToLayer("Enemy")) {
            Explode();
            collision.collider.GetComponent<EnemyController>().Die();
        } 
        else if (layer == LayerMask.NameToLayer("Ground")) {
            Invoke("Explode", 1.5f);
        }
    }



    /**
     * Para destruir el la instancia del ParticleSystem
     * del efecto de la explosion.
     */
    private void OnDestroy() {
        Destroy(explodeEffect);
    }

}
