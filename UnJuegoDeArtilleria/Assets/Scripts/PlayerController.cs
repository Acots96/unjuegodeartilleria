﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Clase que controla el comportamiento del player.
 * 
 * Tras varios intentos fallidos de implementar el nuevo sistema 
 * de Input desarrollado por Unity he decidido implementar 
 * un delegate que elige el metodo de Input a tener en cuenta
 * al iniciar la ejecucion.
 */
public class PlayerController : MonoBehaviour {

    private Animator animator;
    private Rigidbody2D rb;
    private Transform thisTr;
    private SpriteRenderer sr;

    public float jumpForce, speedMultiplier;
    public GameManager Manager;

    public LayerMask layerMaskGround;
    public Transform[] groundCheckers;
    public Transform limitDownChecker;
    private float checkDistance;

    private bool jump, click, canShoot, isCollidingTrigger, isColliding;
    private bool isGrounded, isFacingRight;
    private float speed;
    private Vector3 direction, normalScale, superScale;
    private int coins;

    public GameObject fireBallPrefab;
    public Transform lastCheckPoint { get; private set; }

    private delegate void CheckInputDelegate();
    private CheckInputDelegate CheckInput;

    public bool IsSuper { get; private set; }


    private void Awake() {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        thisTr = transform;
        if (Application.isMobilePlatform)
            CheckInput = CheckMobileInput;
        else
            CheckInput = CheckStandaloneInput;
        normalScale = transform.localScale;
        superScale = normalScale * 1.5f;
        isFacingRight = canShoot = true;
        IsSuper = false;
        checkDistance = Mathf.Abs(limitDownChecker.transform.position.y - groundCheckers[0].transform.position.y);

        //Si algun GO esta seleccionado el personaje se mueve demasiado rapido
        Selection.activeGameObject = null;
    }

    /**
     * Llamado tras morir y aparecer en un CheckPoint que no
     * sea el del principio.
     */
    public void Init() {
        enabled = true;
        foreach (Collider2D c in GetComponents<Collider2D>())
            c.enabled = true;
        IsSuper = false;
        transform.localScale = normalScale;
        checkDistance = Mathf.Abs(limitDownChecker.transform.position.y - groundCheckers[0].transform.position.y);
        animator.SetBool("IsJumping", false);
        animator.SetTrigger("DoRestart");
    }


    /**
     * En primer lugar comprueba el input:
     * - Tecla espaciadora para el salto.
     * - Flechas laterales o las teclas AD para el movimiento.
     * - Click del mouse para disparar si ha pasado un
     * segundo y esta en estado super.
     * 
     * Luego comprueba si debe saltar y/o girarse.
     * 
     * Tambien actualiza la velocidad en el Animator.
     * 
     * Finalmente comprueba si esta sobre el suelo.
     */
    private void Update() {
        CheckInput();
        if (speed != 0 && speed > 0 != isFacingRight)
            Flip();
        if (jump)
            Jump();
        if (click && canShoot && IsSuper)
            Shoot();
        animator.SetFloat("Speed", Mathf.Abs(speed));
        isColliding = isCollidingTrigger = false;
    }
    /**
     * Dado que es el Rigidbody2D el que mueve al jugador
     * debe hacerlo desde el FixedUpdate.
     * Mueve al player segun la velocidad (que incluye el signo),
     * el tiempo transcurrido desde el ultimo frame (el cual hay que controlar
     * porque los primeros frames son demasiado largos) y un multiplicador 
     * para calibrar la velocidad desde el inspector.
     */
    private void FixedUpdate() {
        float deltaTime = Time.fixedDeltaTime > 0.01f ? 0.01f : Time.fixedDeltaTime;
        rb.velocity = new Vector2(speed * deltaTime * speedMultiplier, rb.velocity.y);
    }


    /**
     * Solo salta si esta en el suelo
     * o si ha caido encima de un enemigo y lo ha matado.
     */
    private void Jump(bool force = false) {
        CheckForGround();
        if (!force && !isGrounded)
            return;
        Manager.PlayJumpSound();
        rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        //rb.velocity = new Vector2(rb.velocity.x, 10);
        jump = false;
        animator.SetBool("IsJumping", true);
    }
    /**
     * Hace un flip de la animacion cuando
     * el player cambia de direccion.
     */
    private void Flip() {
        isFacingRight = !isFacingRight;
        sr.flipX = !sr.flipX;
    }
    /**
     * Instancia el proyectil que lanza mario en estado Super
     * Lo instancia delante de el, teniendo en cuenta el lado
     * hacia el que mira.
     * Finalmente invoca un metodo que espera un segundo para
     * permitir volver a disparar
     */
    private void Shoot() {
        canShoot = false;
        GameObject fb = Instantiate(fireBallPrefab, 
            thisTr.position + Vector3.right * (isFacingRight ? 1 : -1) * 0.25f, 
            Quaternion.LookRotation(isFacingRight ? Vector3.forward : -Vector3.forward));
        Invoke("WaitToShootAgain", 1f);
    }
    private void WaitToShootAgain() {
        canShoot = true;
    }



    /**
     * Gana al tocar el collider de la puerta
     * del castillo.
     */
    private void Win() {
        enabled = false;
        Manager.Win();
        gameObject.SetActive(false);
    }
    /**
     * Pierde al caer de las plataformas del suelo
     * o al tocar a un enemigo lateralmente sin ser super.
     */
    public void Die() {
        enabled = false;
        animator.SetTrigger("DoDie");
        animator.SetBool("IsJumping", false);
        rb.velocity = Vector2.zero;
        rb.AddForce(new Vector2(0, jumpForce * 0.75f), ForceMode2D.Impulse);
        foreach (Collider2D c in GetComponents<Collider2D>())
            c.enabled = false;
        Manager.Die();
    }




    /**
     * - Al colisionar contra cualquier collider considerado
     * como suelo se comprueba que sea verdaderamente suelo 
     * (debajo del player en lugar de al lado).
     * 
     * - Si por otro lado colisiona con el collider que hay
     * en las zonas sin bloques de suelo muere.
     */
    private void OnCollisionEnter2D(Collision2D collision) {
        if (isCollidingTrigger)
            return;
        isCollidingTrigger = true;
        int layer = collision.gameObject.layer;
        if (layer == LayerMask.NameToLayer("Ground")) {
            CheckForGround();
        } 
        else if (layer == LayerMask.NameToLayer("Die")) {
            Die();
        }
    }
    /**
     * Cuando ya no colisiona con el suelo.
     */
    private void OnCollisionExit2D(Collision2D collision) {
        int layer = collision.gameObject.layer;
        if (layer == LayerMask.NameToLayer("Ground")) {
            isGrounded = false;
        }
    }


    /**
     * - Si el player toca una moneda se le suma.
     * - Si toca un powerup se le aumenta la scale y
     * es invencible.
     * 
     * - Si toca un enemigo se consideran mas factores, 
     * como por ejemplo si es super, en cuyo caso muere el enemigo.
     * Si por el contrario no es super entonces se le pregunta al enemigo
     * el collider con el que ha colisionado y se actua en consecuencia
     * (muere si toca un lateral, rebota si ha tocado el superior).
     * 
     * - Si toca el collider de la bandera esta reproduce la animacion
     * de descender hasta la parte inferior.
     * - Si llega a la puerta negra del castillo gana.
     * 
     * - Si toca un CheckPoint lo guarda para poder revivir al morir si
     * es el primero que toca o si esta mas avanzado que el ultimo
     * que haya tocado.
     */
    private void OnTriggerEnter2D(Collider2D other) {
        if (isColliding)
            return;
        isColliding = true;
        int layer = other.gameObject.layer;
        if (layer == LayerMask.NameToLayer("Coin")) {
            Manager.AddCoin();
            Destroy(other.gameObject);
        }
        else if (layer == LayerMask.NameToLayer("Powerup")) {
            transform.localScale = superScale;
            IsSuper = true;
            StartCoroutine(BlinkingEffect(true));
            checkDistance = Mathf.Abs(limitDownChecker.transform.position.y - groundCheckers[0].transform.position.y);
            Manager.CatchPowerup();
            Destroy(other.gameObject);
        }
        else if (layer == LayerMask.NameToLayer("Enemy")) {
            EnemyController ec = other.transform.parent.GetComponent<EnemyController>();
            if (ec.IsDead)
                return;
            if (ec.IsDeadlyCollider(other)) {
                if (IsSuper) {
                    ec.Die();
                    Manager.KillEnemy();
                    transform.localScale = normalScale;
                    checkDistance = Mathf.Abs(limitDownChecker.transform.position.y - groundCheckers[0].transform.position.y);
                    StartCoroutine(BlinkingEffect(false));
                } else {
                    Die();
                }
            } else {
                Jump(true);
            }
        }
        else if (layer == LayerMask.NameToLayer("Flag")) {
            Manager.FlagDown();
        }
        else if (layer == LayerMask.NameToLayer("Win")) {
            Win();
        }
        else if (layer == LayerMask.NameToLayer("CheckPoint")) {
            if (lastCheckPoint == null) {
                lastCheckPoint = other.transform;
            } else {
                if (other.transform.position.x > lastCheckPoint.transform.position.x)
                    lastCheckPoint = other.transform;
            }
        }
    }



    /**
     * Metodos para comprobar el Input.
     */
    private void CheckStandaloneInput() {
        jump = Input.GetButtonDown("Jump");
        speed = Input.GetAxisRaw("Horizontal");
        click = Input.GetMouseButtonUp(0);
    }
    private void CheckMobileInput() {
        //Android?
    }



    /**
     * Utiliza RaycastHit2D desde dos posiciones en este caso
     * (extremos laterales del player) para comprobar
     * si esta en el suelo.
     */
    private void CheckForGround() {
        foreach (Transform tr in groundCheckers) {
            RaycastHit2D hit = Physics2D.Raycast(tr.position, Vector2.down, checkDistance, layerMaskGround.value);
            if (hit.transform) {
                isGrounded = true;
                animator.SetBool("IsJumping", false);
                break;
            }
        }
    }



    private IEnumerator BlinkingEffect(bool b) {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        float t = Time.time + 1.5f;
        while (t > Time.time) {
            sr.enabled = !sr.enabled;
            yield return new WaitForSeconds(0.05f);
        }
        sr.enabled = true;
        IsSuper = b;
    }

}
